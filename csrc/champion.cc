#include "actions_generator.hh"
#include "model-client.hh"
#include "api.hh"
#include "utils.hh"
#include <algorithm>
#include <chrono>
#include <cstdio>
#include <cassert>
#include <ios>
#include <fstream>
#include <cmath>
#include <unordered_set>
#include <chrono>
using namespace std;

void partie_init()
{
    DEBUG = false;
    PRINTF("---------- INIT (%d) ----------\n", moi());
    cached_api::init();
    model_server::client::connect();
}

void jouer_tour()
{
    PRINTF("---------- Turn %d (%d) ----------\n", tour_actuel(), moi());
    cached_api::clear_cache();

    if (moi() == 0 && tour_actuel() % 5 == 0)
        afficher_etablis();

    while (!a_donne_echantillon())
    {
        cached_api::clear_cache();
        model_server::client::new_state();
        gen_possible_actions();
        auto a = model_server::client::choose_transition();
        cached_api::perform_actions(a);
    }

    if (!a_pose_echantillon())
    {
        afficher_etablis();
        for (int i = 0; i < 50; i++)
            puts("!!!  Pas posé !!!");
    }

    if (tour_actuel() == NB_TOURS)
        afficher_etablis();
}

void partie_fin()
{
    PRINTF("---------- END (%d) ----------\n", moi());
    cached_api::clear_cache();
    model_server::client::disconnect();
}
