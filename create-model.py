import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.jit as jit
import os
from typing import Final


i_metadata: Final[int] = 36
h1: Final[int] = 256
h2: Final[int] = 128
in_c: Final[int] = 6
out_c: Final[int] = 16


class MyModel(nn.Module):

    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_c, out_c, 3, padding=0)
        # self.conv2 = nn.Conv2d(out_c, out_c, 3, padding=0)
        self.c_h1 = nn.Linear(2 * 3*3*out_c + i_metadata, h1)
        self.h1_h2 = nn.Linear(h1, h2)
        self.h2_out = nn.Linear(h2, 1)

    # @jit.script_method
    def forward(self, metadata, b0, b1):
        c_b0 = torch.tanh(self.conv1(b0))
        c_b1 = torch.tanh(self.conv1(b1))
        
        # c_b0 = torch.tanh(self.conv2(c_b0))
        # c_b1 = torch.tanh(self.conv2(c_b1))

        pool_b0 = torch.tanh(F.avg_pool2d(c_b0, 2, stride=1).flatten(1))
        pool_b1 = torch.tanh(F.avg_pool2d(c_b1, 2, stride=1).flatten(1))

        cat = torch.cat([metadata, pool_b0, pool_b1], dim=1)
        h = torch.tanh(self.c_h1(cat))
        h = torch.tanh(self.h1_h2(h))
        out = self.h2_out(h)
        return out.squeeze(1)


def save_model(model, filename):
    model = model.cuda()
    metadata = torch.randn(8, i_metadata).cuda()
    board = torch.randn(8, in_c, 6, 6).cuda()
    jit_model = jit.trace(model, (metadata, board, board))
    # jit_model = jit.script(model)
    jit_model.save(filename)
    print(f'Model saved to {filename}:')
    print(jit_model.code)
    # model.forward(metadata, board, board)

    n_params = sum(p.numel()
                   for p in jit_model.parameters() if p.requires_grad)
    print(f'\n\n{n_params} parameters')


save_model(MyModel(), 'model.pt')
os.remove('training.stats')
