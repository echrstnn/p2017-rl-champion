#pragma once

#include <tuple>
#include <utility>
#include <vector>
#include <stack>
#include "api_.hh"

extern bool DEBUG;
#define PRINTF(...)                 \
    {                               \
        if (DEBUG)                  \
        {                           \
            printf("[%d] ", moi()); \
            printf(__VA_ARGS__);    \
        }                           \
    }

const int NUM_ELEMENTS = 5;
const case_type ELEMENTS[NUM_ELEMENTS] = {PLOMB, FER, CUIVRE, SOUFRE, MERCURE};
extern position POS[TAILLE_ETABLI*TAILLE_ETABLI];

inline position i_to_pos(int pos_id) { return {pos_id / TAILLE_ETABLI, pos_id % TAILLE_ETABLI}; }
inline int pos_to_i(position pos) { return TAILLE_ETABLI * pos.ligne + pos.colonne; }

inline bool on_board(position pos)
{
    return 0 <= pos.ligne && pos.ligne < TAILLE_ETABLI && 0 <= pos.colonne && pos.colonne < TAILLE_ETABLI;
}

namespace cached_api
{
    struct action_node
    {
        int parent_id;
        action_type type;
        union action_params
        {
            std::pair<position, position> place;
            position transmute;
            std::tuple<position, int, case_type, case_type> catalyze;
            echantillon give;
        } params;
    };

    extern int CACHE_EPOCH;
    extern int _moi;
    extern int _adversaire;
    extern int _score[2];
    extern int epoch_score[2];
    extern int num_earned_catalyzers;
    extern int num_spent_catalyzers;
    extern echantillon _echantillon_tour;
    extern int score_at_turn_begin;

    void init();
    void clear_cache();

    erreur hook_placer_echantillon(position pos1, position pos2);
    erreur hook_transmuter(position pos);
    erreur hook_catalyser(position pos, int id_apprenti, case_type terrain);
    erreur hook_donner_echantillon(echantillon echantillon_donne);
    bool hook_annuler();

    bool has_last_action_node();
    const action_node &last_action_node();
    int last_action_node_id();
    int num_action_nodes();

    case_type cached_type_case(position pos, int id_apprenti);
    int cached_score(int player);

    void perform_actions(int node_id);

    void dump_workbench(int player, uint8_t *out);
}

#define placer_echantillon(pos1, pos2) cached_api::hook_placer_echantillon(pos1, pos2)
#define transmuter(pos) cached_api::hook_transmuter(pos)
#define catalyser(pos, p, elt) cached_api::hook_catalyser(pos, p, elt)
#define donner_echantillon(ech) cached_api::hook_donner_echantillon(ech)
#define annuler() cached_api::hook_annuler()

#define type_case(pos, p) cached_api::cached_type_case(pos, p)
#define moi() cached_api::_moi
#define adversaire() cached_api::_adversaire
#define score(player) cached_api::cached_score(player)
#define nombre_catalyseurs() (cached_api::num_earned_catalyzers - cached_api::num_spent_catalyzers)
#define echantillon_tour() cached_api::_echantillon_tour