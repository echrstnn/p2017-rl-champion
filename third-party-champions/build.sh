#!/bin/sh
DIRS="basic_greedy full_greedy simple_greedy uriopass"

for dir in $DIRS
do
    cd $dir
    make clean
    cd ..
done

for dir in $DIRS
do
    cd $dir
    make
    cd ..
done