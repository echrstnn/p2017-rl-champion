#include <assert.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include "actions_generator.hh"
#include "model-client.hh"
#include "utils.hh"
using namespace model_server;

const char *CLIENT_PATH_CMD = "../model-cmd";
const char *CLIENT_PATH_OUT = "./model-out";

int fd_cmd, fd_out;
bool initial_state;

inline void fill_state(state &s)
{
    s.turn = tour_actuel();
    s.playing = moi();
    s.num_catalyzers = nombre_catalyseurs();

    s.sample[adversaire()][0] = echantillon_tour().element1;
    s.sample[adversaire()][1] = echantillon_tour().element2;
    s.sample[moi()][0] = echantillon_tour().element1;
    s.sample[moi()][1] = echantillon_tour().element2;
    if (a_pose_echantillon())
        s.sample[moi()][0] = VIDE, s.sample[moi()][1] = VIDE;

    if (cached_api::has_last_action_node()) // Si on envoie une transition
    {
        const auto &node = cached_api::last_action_node();
        if (node.type == ACTION_DONNER_ECHANTILLON)
        {
            s.turn++, s.playing = adversaire(), s.num_catalyzers = 0;
            s.sample[adversaire()][0] = node.params.give.element1;
            s.sample[adversaire()][1] = node.params.give.element2;
            s.sample[moi()][0] = node.params.give.element1;
            s.sample[moi()][1] = node.params.give.element2;
        }
    }

    cached_api::dump_workbench(0, s.board[0]);
    cached_api::dump_workbench(1, s.board[1]);
    s.score[0] = score(0), s.score[1] = score(1);
}

void client::connect()
{
    PRINTF("Connecting to model server...\n");
    fd_cmd = open(CLIENT_PATH_CMD, O_WRONLY);
    assert(fd_cmd != -1);

    if (moi() == 0)
    {
        command cmd;
        cmd.type = BEGIN_EPISODE;
        fill_state(cmd.new_state);
        write_struct(fd_cmd, &cmd);
    }

    puts("Dans la file d'attente...");
    fd_out = open(CLIENT_PATH_OUT, O_RDONLY);
    assert(fd_out != -1);

    initial_state = true;
    PRINTF("Connected !\n");
}

void client::new_state()
{
    if (tour_actuel() == 1 && initial_state)
        return;
    command cmd;
    cmd.type = NEW_STATE;
    fill_state(cmd.new_state);

    write_struct(fd_cmd, &cmd);
}

void client::new_transition(int id)
{
    command cmd;
    cmd.type = NEW_TRANSITION;
    cmd.new_transition.id = id;
    fill_state(cmd.new_transition.new_state);

    write_struct(fd_cmd, &cmd);
}

int client::choose_transition()
{
    initial_state = false;

    command cmd;
    cmd.type = CHOOSE_TRANSITION;
    write_struct(fd_cmd, &cmd);

    int id;
    read_struct(fd_out, &id);
    return id;
}

void client::disconnect()
{
    if (moi() == 0)
    {
        command cmd;
        cmd.type = END_EPISODE;
        fill_state(cmd.new_state);
        write_struct(fd_cmd, &cmd);
    }

    close(fd_cmd);
    close(fd_out);
}