#include <assert.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdlib.h>
#include <cstdio>
#include <fstream>
#include <chrono>
#include <cmath>
#include "model-server.hh"
#include "torch-model.hh"
using namespace model_server;

const char *model_server::PATH_CMD = "./slots/model-cmd";
const char *model_server::PATH_OUT[2] = {"./slots/s0/model-out", "./slots/s1/model-out"};
const char *PATH_STATS = "training.stats";

// Descripteurs de fichiers
int fd_cmd, fd_out;

const char *model_server::config::PATH = "model-server.config";
bool model_server::config::train;
double model_server::config::discount_factor;
double model_server::config::learning_rate;
bool model_server::config::deterministic[2];
double model_server::config::epsilon;
double model_server::config::score_factor;
int model_server::config::batch_size;
int model_server::config::sgd_step_skip;
int model_server::config::rmem_size;

void model_server::config::load()
{
    const int IGN_SZ = 1024;
    std::ifstream cfg(config::PATH);
    cfg >> config::train;
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::discount_factor;
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::learning_rate;
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::deterministic[0] >> config::deterministic[1];
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::epsilon;
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::score_factor;
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::batch_size;
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::sgd_step_skip;
    cfg.ignore(IGN_SZ, '\n');
    cfg >> config::rmem_size;
    printf("train=%d  discountf=%.3f  lr=%.4f  determ=[%d,%d]  eps=%.3f  scoref=%.0f  batchsz=%d  sgdskip=%d  rmem=%d\n",
           (int)config::train, config::discount_factor, config::learning_rate,
           (int)config::deterministic[0], (int)config::deterministic[1],
           config::epsilon, config::score_factor, config::batch_size, config::sgd_step_skip, config::rmem_size);
}

int main()
{
    mkfifo(PATH_CMD, 0666);
    mkfifo(PATH_OUT[0], 0666);
    mkfifo(PATH_OUT[1], 0666);

    model_load();
    config::load();

    puts("\nWaiting for a client...");
    fd_cmd = open(PATH_CMD, O_RDONLY);

    int num_episodes = 0;
    command cmd, cmd_waiting;
    int slot = 0;
    bool other_ready = false, busy = false;

    while (true)
    {
        if (other_ready && !busy)
            cmd = cmd_waiting, slot = 1 - slot;
        else
            read_struct(fd_cmd, &cmd);

        int tr_id;
        switch (cmd.type)
        {
        case BEGIN_EPISODE:
            if (!busy)
            {
                busy = true, other_ready = false;
                fd_out = open(PATH_OUT[slot], O_WRONLY);
                model_new_episode(cmd.new_state);
            }
            else
            {
                other_ready = true;
                cmd_waiting = cmd;
            }
            break;

        case NEW_STATE:
            // if(cmd.new_state.turn == 100)
            //     Profiler::enable();
            // if(cmd.new_state.turn == 103)
            //     Profiler::disable();
            model_new_state(cmd.new_state);
            break;

        case NEW_TRANSITION:
            model_new_transition(cmd.new_transition);
            break;

        case CHOOSE_TRANSITION:
            tr_id = model_choose_transition();
            write_struct(fd_out, &tr_id);
            break;

        case END_EPISODE:
            close(fd_out);
            model_end_episode(cmd.new_state);

            if (++num_episodes % 10 == 0)
            {
                model_adjust_lr();
                model_show_stats();
                model_save();
                config::load();
            }
            if (config::train)
            {
                std::ofstream stats(PATH_STATS, std::ios_base::app | std::ios_base::out);
                stats << cmd.new_state.score[0] << " " << cmd.new_state.score[1] << "\n";
            }
            busy = false;
            if (!other_ready)
            {
                close(fd_cmd);
                fd_cmd = open(PATH_CMD, O_RDONLY);
                assert(fd_cmd != -1);
            }
            break;

        default:
            assert(false);
        }
    }
}