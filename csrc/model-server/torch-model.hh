#pragma once

#include "model-server.hh"

namespace Profiler
{
    void enable();
    void disable();
}

void model_load();
void model_save();

void model_new_episode(model_server::state &initial_state);
void model_new_state(model_server::state &new_state);
void model_new_transition(model_server::transition &new_transition);
int model_choose_transition();
void model_end_episode(model_server::state &final_state);

void model_adjust_lr();
void model_show_stats();
