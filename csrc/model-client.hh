#pragma once

#include "model-server/model-server.hh"

namespace model_server {
    namespace client {
        void connect();
        void disconnect();

        void new_state();
        void new_transition(int id);
        int choose_transition();
    }
}