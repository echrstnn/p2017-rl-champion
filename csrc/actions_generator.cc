#include "actions_generator.hh"
#include "model-client.hh"
#include "utils.hh"
#include <cassert>
#include <vector>
#include <algorithm>
using namespace std;

int num_boards;

const int DEPS[4][2] = {{+1, 0}, {0, +1}, {-1, 0}, {0, -1}};

inline void send_transition()
{
    model_server::client::new_transition(cached_api::last_action_node_id());
    num_boards++;
}

void gen_place()
{
    for (auto p : placements_possible_echantillon(echantillon_tour(), moi()))
    {
        placer_echantillon(p.pos1, p.pos2);
        send_transition();
        annuler();
    }
}

std::vector<std::pair<position, int>> regions;

void gen_transmute_place(int tr, int min_sz, int min_id = 0)
{
    if (tr == 0)
    {
        gen_place();
        return;
    }

    for (int i = min_id; i < (int)regions.size(); i++)
    {
        auto reg_pos = regions[i].first;
        auto reg_size = regions[i].second;
        if (reg_size >= min_sz)
        {
            transmuter(reg_pos);
            gen_transmute_place(tr - 1, min_sz, i + 1);
            annuler();
        }
    }
}

void gen_possible_actions()
{
    num_boards = 0;
    regions = compute_regions();

    if (!a_pose_echantillon())
    {
        gen_place();

        if (num_boards == 0)
        {
            gen_transmute_place(1, 2);
            gen_transmute_place(2, 2);

            if (num_boards == 0)
                gen_transmute_place(1, 1);
            if (num_boards == 0)
                gen_transmute_place(2, 1);
        }
    }

    if (nombre_catalyseurs() > 0)
    {
        bool elt_ok[NB_TYPE_CASES];
        for (int player = 0; player < 2; player++)
            for (auto pos : POS)
            {
                case_type curr_elem = type_case(pos, player);
                if (curr_elem != VIDE)
                {
                    if (player == moi())
                    {
                        std::fill(elt_ok, elt_ok + NB_TYPE_CASES, false);
                        for (auto dep : DEPS)
                        {
                            position new_pos = {pos.ligne + dep[0], pos.colonne + dep[1]};
                            if (on_board(new_pos))
                                elt_ok[type_case(new_pos, player)] = true;
                        }
                    }
                    else
                        std::fill(elt_ok, elt_ok + NB_TYPE_CASES, true);

                    elt_ok[curr_elem] = false;

                    for (int i = 1; i < NB_TYPE_CASES; i++)
                        if (elt_ok[i])
                        {
                            case_type target_elem = (case_type)i;
                            catalyser(pos, player, target_elem);
                            send_transition();
                            annuler();
                        }
                }
            }
    }

    if (a_pose_echantillon() || num_boards == 0)
    {
        echantillon my_sample = echantillon_tour();
        for (int elem1_id = 0; elem1_id < NUM_ELEMENTS; elem1_id++)
            for (int elem2_id = elem1_id; elem2_id < NUM_ELEMENTS; elem2_id++)
            {
                auto elem1 = ELEMENTS[elem1_id], elem2 = ELEMENTS[elem2_id];
                if (elem1 == my_sample.element1 || elem1 == my_sample.element2 || elem2 == my_sample.element1 || elem2 == my_sample.element2)
                {
                    echantillon to_give = {elem1, elem2};
                    donner_echantillon(to_give);
                    send_transition();
                    annuler();
                }
            }
    }

    PRINTF("%d actions generated resulting in %d boards\n", cached_api::num_action_nodes(), num_boards);
}