import matplotlib.pyplot as plt
import numpy as np

with open('training.stats') as file:
    lines = [line.rstrip() for line in file]

score = list(map(lambda l: list(map(int, l.split(' '))), lines))
score0 = [s[0] for s in score]
score1 = [s[1] for s in score]
absd = [ np.abs(s0-s1) for s0, s1 in score]
win0 = [ 1 if s0>s1 else (0 if s1>s0 else 0.5) for s0, s1 in score]
X = list(range(len(score)))

def plotsmooth(yref, label, color, decay = 0.99, otheraxis=False, alpha=1, char='-'):
    Ysmooth = [0]
    for y in yref[1:]:
        Ysmooth.append(decay*Ysmooth[-1] + (1-decay)*y)
    ax = plt
    if otheraxis:
        ax = plt.twinx()
        ax.set_ylim(0, 1)
    ax.plot(X, Ysmooth, char, label=label, color=color, alpha=alpha)

def plotfit(yref, label, color):
    a, b = np.polyfit(X, yref, 1)
    Yreg = list(map(lambda x: a*x + b, X))
    plt.plot(X, Yreg, label=label, color=color, alpha=0.5)

plt.plot(score0, '.', label='M0', color='red', alpha=0.05)
plt.plot(score1, '.', label='M1', color='blue', alpha=0.05)
# plotfit(score0, 'fit0', 'red')
# plotfit(score1, 'fit1', 'blue')
plotsmooth(score0, 'smoothM0', 'red')
plotsmooth(score1, 'smoothM1', 'blue')
# plotsmooth(absd, 'absd', 'green', 0.8)
plotsmooth(win0, 'winrate0', 'green', 0.99, True, alpha=0.3, char='-')
plt.legend()
plt.show()
