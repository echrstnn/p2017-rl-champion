#include <cassert>
#include <stack>
#include <deque>
#include <algorithm>
#include "api.hh"
using namespace std;
using namespace cached_api;

struct workbench
{
    int curr_epoch;
    bool can_dump;
    uint8_t value[TAILLE_ETABLI][TAILLE_ETABLI];
    int last_epoch[TAILLE_ETABLI][TAILLE_ETABLI];
};

workbench player_workbench[2];

int cached_api::CACHE_EPOCH;
int cached_api::_moi, cached_api::_adversaire;
echantillon cached_api::_echantillon_tour;
int cached_api::_score[2];
int cached_api::epoch_score[2];
int cached_api::num_earned_catalyzers;
int cached_api::num_spent_catalyzers;
int cached_api::score_at_turn_begin;

bool DEBUG = true;
const char *ELEM_STR[] = {"__", "Pb", "Fe", "Cu", " S", "Hg"};
position POS[TAILLE_ETABLI * TAILLE_ETABLI];

vector<action_node> node_list;
stack<int> node_id_stack;

// On undef pour pouvoir utiliser les vraies fonctions pour implémenter les hooks
#undef placer_echantillon
#undef transmuter
#undef catalyser
#undef donner_echantillon
#undef annuler
#undef type_case
#undef moi
#undef adversaire
#undef score
#undef nombre_catalyseurs
#undef echantillon_tour

void cached_api::init()
{
    _moi = moi(), _adversaire = adversaire();
    node_id_stack.push(-1);
    clear_cache();
    for (int lig = 0; lig < TAILLE_ETABLI; lig++)
        for (int col = 0; col < TAILLE_ETABLI; col++)
            POS[pos_to_i({lig, col})] = {lig, col};
}

void cached_api::clear_cache()
{
    CACHE_EPOCH++;
    player_workbench[0].curr_epoch++;
    player_workbench[1].curr_epoch++;
    node_list.clear();
    while (node_id_stack.top() != -1)
        node_id_stack.pop();
    num_earned_catalyzers = nombre_catalyseurs();
    num_spent_catalyzers = 0;
    _echantillon_tour = echantillon_tour();
    score_at_turn_begin = score(moi());
}

inline void invalidate_cache_because(const action_node &node)
{
    position pos1, pos2;
    int player;
    case_type elt, elt_prev;
    workbench *wb;
    switch (node.type)
    {
    case ACTION_PLACER:
        pos1 = node.params.place.first, pos2 = node.params.place.second;
        wb = &player_workbench[_moi];
        wb->value[pos1.ligne][pos1.colonne] = _echantillon_tour.element1;
        wb->value[pos2.ligne][pos2.colonne] = _echantillon_tour.element2;
        wb->last_epoch[pos1.ligne][pos1.colonne] = wb->curr_epoch;
        wb->last_epoch[pos2.ligne][pos2.colonne] = wb->curr_epoch;
        break;
    case ACTION_TRANSMUTER:
        CACHE_EPOCH++;
        wb = &player_workbench[_moi];
        wb->curr_epoch++;
        wb->can_dump = false;
        num_earned_catalyzers = nombre_catalyseurs();
        break;
    case ACTION_CATALYSER:
        tie(pos1, player, elt, elt_prev) = node.params.catalyze;
        wb = &player_workbench[player];
        wb->value[pos1.ligne][pos1.colonne] = elt;
        wb->last_epoch[pos1.ligne][pos1.colonne] = wb->curr_epoch;
        num_spent_catalyzers++;
    default:
        break;
    }
}

inline void invalidate_cache_because_undo(const action_node &node)
{
    position pos1, pos2;
    int player;
    case_type elt, elt_prev;
    workbench *wb;
    switch (node.type)
    {
    case ACTION_PLACER:
        pos1 = node.params.place.first, pos2 = node.params.place.second;
        wb = &player_workbench[_moi];
        wb->value[pos1.ligne][pos1.colonne] = VIDE;
        wb->value[pos2.ligne][pos2.colonne] = VIDE;
        wb->last_epoch[pos1.ligne][pos1.colonne] = wb->curr_epoch;
        wb->last_epoch[pos2.ligne][pos2.colonne] = wb->curr_epoch;
        break;
    case ACTION_CATALYSER:
        tie(pos1, player, elt, elt_prev) = node.params.catalyze;
        wb = &player_workbench[player];
        wb->value[pos1.ligne][pos1.colonne] = elt_prev;
        wb->last_epoch[pos1.ligne][pos1.colonne] = wb->curr_epoch;
        num_spent_catalyzers--;
        break;
    default:
        invalidate_cache_because(node);
        break;
    }
}

inline void push_action_node(action_type type, action_node::action_params params)
{
    int parent_id = node_id_stack.top();
    node_id_stack.push(node_list.size());
    node_list.push_back({parent_id, type, params});
    invalidate_cache_because(node_list.back());
}

erreur cached_api::hook_placer_echantillon(position pos1, position pos2)
{
    // Ne pas utiliser l'API pour gagner un peu en performance
    // auto err = placer_echantillon(pos1, pos2);
    // assert(err == OK);
    assert(cached_type_case(pos1, _moi) == VIDE && cached_type_case(pos2, _moi) == VIDE);
    push_action_node(ACTION_PLACER, {.place = make_pair(pos1, pos2)});
    return OK;
}

erreur cached_api::hook_transmuter(position pos)
{
    auto err = transmuter(pos);
    assert(err == OK);
    push_action_node(ACTION_TRANSMUTER, {.transmute = pos});
    return err;
}

erreur cached_api::hook_catalyser(position pos, int player, case_type target_elem)
{
    // Ne pas utiliser l'API pour gagner un peu en performance
    // auto err = donner_echantillon(sample);
    // assert(err == OK);
    auto old_elt = cached_type_case(pos, player);
    assert(old_elt != VIDE && num_earned_catalyzers - num_spent_catalyzers > 0);
    push_action_node(ACTION_CATALYSER, {.catalyze = make_tuple(pos, player, target_elem, old_elt)});
    return OK;
}

erreur cached_api::hook_donner_echantillon(echantillon sample)
{
    // Comme ça n'a aucun effet sur le plateau on peut ne pas utiliser la vraie fonction
    // Cela permet un gain de performance de +20% en profilant
    // auto err = donner_echantillon(sample);
    // assert(err == OK);
    push_action_node(ACTION_DONNER_ECHANTILLON, {.give = sample});
    return OK;
}

bool cached_api::hook_annuler()
{
    const auto &node = last_action_node();
    bool err;
    switch (node.type)
    {
    case ACTION_PLACER:
    case ACTION_CATALYSER:
    case ACTION_DONNER_ECHANTILLON:
        err = true;
        break;

    default:
        err = annuler();
        assert(err);
        break;
    }
    invalidate_cache_because_undo(node);
    node_id_stack.pop();
    return err;
}

bool cached_api::has_last_action_node() { return last_action_node_id() != -1; }
const action_node &cached_api::last_action_node() { return node_list[last_action_node_id()]; }
int cached_api::last_action_node_id() { return node_id_stack.top(); }

int cached_api::num_action_nodes() { return node_list.size(); }

case_type cached_api::cached_type_case(position pos, int id_apprenti)
{
    assert(on_board(pos));
    workbench *wb = &player_workbench[id_apprenti];
    if (wb->last_epoch[pos.ligne][pos.colonne] != wb->curr_epoch)
    {
        wb->last_epoch[pos.ligne][pos.colonne] = wb->curr_epoch;
        wb->value[pos.ligne][pos.colonne] = type_case(pos, id_apprenti);
    }
    return (case_type)wb->value[pos.ligne][pos.colonne];
}

int cached_api::cached_score(int player)
{
    if (epoch_score[player] != CACHE_EPOCH)
    {
        epoch_score[player] = CACHE_EPOCH;
        _score[player] = score(player);
    }
    return _score[player];
}

void cached_api::perform_actions(int node_id)
{
    if (node_id == -1)
        return;

    const auto &node = node_list[node_id];
    perform_actions(node.parent_id);

    position pos1, pos2;
    int player;
    case_type elt, elt_prev;
    echantillon sample;
    erreur err;
    switch (node.type)
    {
    case ACTION_PLACER:
        pos1 = node.params.place.first, pos2 = node.params.place.second;
        PRINTF("PLACE <%d %d> <%d %d>\n", pos1.ligne, pos1.colonne, pos2.ligne, pos2.colonne);
        err = placer_echantillon(pos1, pos2);
        assert(err == OK);
        break;
    case ACTION_TRANSMUTER:
        pos1 = node.params.transmute;
        PRINTF("TRANSMUTE <%d %d>\n", pos1.ligne, pos1.colonne);
        err = transmuter(pos1);
        assert(err == OK);
        break;
    case ACTION_CATALYSER:
        tie(pos1, player, elt, elt_prev) = node.params.catalyze;
        PRINTF("CATALYZE <%d %d> <%d> <%s -> %s>\n", pos1.ligne, pos1.colonne, player, ELEM_STR[elt_prev], ELEM_STR[elt]);
        err = catalyser(pos1, player, elt);
        assert(err == OK);
        break;
    case ACTION_DONNER_ECHANTILLON:
        sample = node.params.give;
        PRINTF("GIVE <%s> <%s>\n", ELEM_STR[sample.element1], ELEM_STR[sample.element2]);
        err = donner_echantillon(sample);
        assert(err == OK);
        break;
    default:
        assert(false);
    }
}

void cached_api::dump_workbench(int player, uint8_t *out)
{
    workbench *wb = &player_workbench[player];
    if (!wb->can_dump)
    {
        for (position pos : POS)
            cached_type_case(pos, player);
        wb->can_dump = true;
    }
    copy(&wb->value[0][0], &wb->value[0][0] + TAILLE_ETABLI * TAILLE_ETABLI, out);
}