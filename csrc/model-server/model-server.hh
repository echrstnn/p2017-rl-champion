#pragma once

#include <stdint.h>

#define read_struct(fd, s) assert(read(fd, s, sizeof(*(s))) == sizeof(*(s)))
#define write_struct(fd, s) assert(write(fd, s, sizeof(*(s))) == sizeof(*(s)))

namespace model_server
{
    extern const char *PATH_CMD;
    extern const char *PATH_OUT[2];

    const int BOARD_SIZE = 6*6;

    enum command_type
    {
        BEGIN_EPISODE,
        NEW_STATE,
        NEW_TRANSITION,
        CHOOSE_TRANSITION,
        END_EPISODE,
    };

    struct state
    {
        uint8_t turn;
        uint8_t playing;
        uint8_t num_catalyzers;
        uint8_t sample[2][2];
        uint8_t board[2][BOARD_SIZE];
        int16_t score[2];
    };

    struct transition
    {
        int32_t id;
        state new_state;
    };

    struct command
    {
        command_type type;
        union
        {
            state new_state;
            transition new_transition;
        };
    };

    namespace config
    {
        extern const char *PATH;
        void load();

        extern bool train;
        extern double discount_factor;
        extern double learning_rate;
        extern bool deterministic[2];
        extern double epsilon;
        extern double score_factor;
        extern int batch_size;
        extern int sgd_step_skip;
        extern int rmem_size;
    }
}