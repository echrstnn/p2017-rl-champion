#include <random>
#include <optional>
#include <cassert>
#include <unordered_map>
#include "torch-include.hh"
#include "torch-model.hh"
#include "BS_thread_pool_light.hpp"
#include "../api_.hh"
using namespace model_server;

namespace Profiler
{
    namespace tprofiler = torch::autograd::profiler;
    bool enabled = false;

    void enable()
    {
        if (enabled)
            return;
        enabled = true;
        puts("Enabling profiler...");
        auto config = tprofiler::ProfilerConfig(tprofiler::ProfilerState::KINETO);
        std::set<tprofiler::ActivityType> activities{tprofiler::ActivityType::CPU, tprofiler::ActivityType::CUDA};
        std::unordered_set<at::RecordScope> scopes;
        for (int i = 0; i < (int)at::RecordScope::NUM_SCOPES; i++)
            scopes.insert((at::RecordScope)i);
        tprofiler::prepareProfiler(config, activities);
        tprofiler::enableProfiler(config, activities, scopes);
    }

    void disable()
    {
        if (!enabled)
            return;
        enabled = false;
        puts("Disabling profiler...");
        auto results = tprofiler::disableProfiler();
        results->save("prof.out");
    }
}

// Tensor options
auto FCPU = torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCPU);
auto FCUDA = torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA);
auto LCPU = torch::TensorOptions().dtype(torch::kLong).device(torch::kCPU);
auto BCPU = torch::TensorOptions().dtype(torch::kByte).device(torch::kCPU);
auto BCUDA = torch::TensorOptions().dtype(torch::kByte).device(torch::kCUDA);

// Le modèle
torch::jit::Module model;
const char *model_path = "model.pt";

std::unordered_map<std::string, at::Tensor> param_sum_adj;
std::unordered_map<std::string, at::Tensor> param_sum_abs_adj;
std::unordered_map<std::string, at::Tensor> param_lr;

BS::thread_pool_light pool;

std::default_random_engine rng;
std::uniform_real_distribution unif01;

//
// Chargement et sauvegarde du modèle
//

void model_load()
{
    assert(torch::cuda::is_available());
    printf("Using %d threads for the thread pool\n", pool.get_thread_count());
    try
    {
        model = torch::jit::load(model_path);
    }
    catch (const c10::Error &e)
    {
        printf("%s\n", e.what());
        assert(false);
    }

    torch::GradMode::set_enabled(false);
    for (const auto &p : model.named_parameters())
    {
        param_sum_adj[p.name] = torch::zeros_like(p.value);
        param_sum_abs_adj[p.name] = torch::zeros_like(p.value);
        param_lr[p.name] = config::learning_rate * torch::ones_like(p.value);
    }
    puts("Model loaded");
}

void model_save()
{
    model.save(model_path);
    puts("Model saved");
}

struct encoded_state
{
    at::Tensor metadata, board0, board1;
};

inline at::Tensor enc1hot(at::Tensor tens)
{
    auto dims = tens.sizes().vec();
    dims.push_back(NB_TYPE_CASES);
    auto ret = torch::zeros(dims, FCPU);
    ret.scatter_(-1, tens.unsqueeze(-1).to(torch::kLong), 1.f);
    return ret;
}

inline at::Tensor cuda(at::Tensor tens)
{
    return tens.pin_memory().to(torch::kCUDA, true);
}

inline at::Tensor model_forward(encoded_state &s)
{
    return model.forward({s.metadata, s.board0, s.board1}).toTensor();
}

inline encoded_state encode_state(state &s)
{
    static const at::Tensor PLAYING_0 = torch::tensor({1., 0.}, FCPU);
    static const at::Tensor PLAYING_1 = torch::tensor({0., 1.}, FCPU);

    auto turns = torch::zeros({6}, FCPU);
    turns.index_put_({torch::indexing::Slice(0, (1 + s.turn) / 25)}, 1.);
    auto playing = (s.playing == 0 ? PLAYING_0 : PLAYING_1);
    auto catalyzers = torch::zeros({3}, FCPU);
    catalyzers.index_put_({torch::indexing::Slice(0, s.num_catalyzers)}, 1.);
    auto sample = enc1hot(torch::from_blob(s.sample, {2 * 2}, BCPU)).flatten();
    auto b0 = enc1hot(torch::from_blob(s.board[0], {TAILLE_ETABLI, TAILLE_ETABLI}, BCPU)).unsqueeze(0).transpose(1, 3);
    auto b1 = enc1hot(torch::from_blob(s.board[1], {TAILLE_ETABLI, TAILLE_ETABLI}, BCPU)).unsqueeze(0).transpose(1, 3);
    auto score = torch::tensor({(float)(s.score[0] - s.score[1])}, FCPU) / config::score_factor;

    auto metadata = torch::cat({turns, playing, catalyzers, sample, score}, -1).unsqueeze(0);
    return {cuda(metadata), cuda(b0), cuda(b1)};
}

inline encoded_state stack_encoded_states(std::vector<encoded_state> &v)
{
    std::vector<at::Tensor> metadata, b0, b1;
    for (auto &enc_s : v)
    {
        metadata.push_back(enc_s.metadata);
        b0.push_back(enc_s.board0);
        b1.push_back(enc_s.board1);
    }
    return {torch::cat(metadata), torch::cat(b0), torch::cat(b1)};
}

namespace ReplayMemory
{
    struct rmem_entry
    {
        encoded_state s_old;
        at::Tensor stacked_score_delta;
        encoded_state stacked_s_new;
        int stack_size;
        int playing;
    };

    std::vector<rmem_entry> mem_l, mem_r;

    void store(encoded_state &s_old, std::vector<float> &score_delta, std::vector<encoded_state> &s_new, int playing)
    {
        if (s_new.size() == 0) // État final
            mem_l.push_back({.s_old = s_old, .stack_size = 0, .playing = playing});
        else
        {
            int size = s_new.size();
            auto stacked_score_delta = cuda(torch::from_blob(score_delta.data(), {size}, FCPU));
            mem_l.push_back({s_old, stacked_score_delta, stack_encoded_states(s_new), size, playing});
        }

        while ((int)(mem_l.size() + mem_r.size()) > config::rmem_size)
        {
            if (mem_r.size() == 0)
            {
                for (int i = mem_l.size() - 1; i >= 0; i--)
                    mem_r.push_back(mem_l[i]);
                mem_l.clear();
            }
            mem_r.pop_back();
        }
    }

    void sgd_step()
    {
        std::vector<rmem_entry *> batch;
        for (int i = 0; i < config::batch_size; i++)
        {
            int mem_size = mem_l.size() + mem_r.size();
            int index = rng() % mem_size;
            if (index < (int)mem_l.size())
                batch.push_back(&mem_l[index]);
            else
                batch.push_back(&mem_r[index - (int)mem_l.size()]);
        }

        std::vector<encoded_state> bv_s_old, bv_stacked_s_new;
        std::vector<at::Tensor> bv_stacked_score_delta;
        std::vector<long> bv_stack_size;
        std::vector<uint8_t> bv_playing;
        for (auto sample : batch)
        {
            bv_s_old.push_back(sample->s_old);
            bv_playing.push_back(sample->playing);
            if (sample->stack_size > 0)
            {
                bv_stacked_score_delta.push_back(sample->stacked_score_delta);
                bv_stacked_s_new.push_back(sample->stacked_s_new);
                bv_stack_size.push_back(sample->stack_size);
            }
        }
        auto b_s_old = stack_encoded_states(bv_s_old), b_stacked_s_new = stack_encoded_states(bv_stacked_s_new);
        auto b_stacked_score_delta = torch::cat(bv_stacked_score_delta);
        auto b_playing0 = cuda(torch::from_blob(bv_playing.data(), {(int) batch.size()}, BCPU)) == 0;

        for (const auto &param : model.parameters())
            param.mutable_grad().reset();

        {
            torch::AutoGradMode guard(true);
            auto b_val_ests_new = model_forward(b_stacked_s_new);
            auto b_tr_val_ests = b_stacked_score_delta / config::score_factor + config::discount_factor * b_val_ests_new;
            auto b_val_est_old = model_forward(b_s_old);

            std::vector<at::Tensor> splitted = b_tr_val_ests.split(bv_stack_size, 0);
            int splitted_idx = 0;
            std::vector<at::Tensor> to_pad;
            for (int i = 0; i < (int)batch.size(); i++)
            {
                auto sample = batch[i];
                if (sample->stack_size == 0)
                    to_pad.push_back(torch::zeros({1}, FCUDA));
                else
                    to_pad.push_back(splitted[splitted_idx++]);
            }
            assert(splitted_idx == (int)splitted.size());

            auto padded = torch::pad_sequence(to_pad, true, NAN);
            auto max_p = std::get<0>(padded.nan_to_num(-INFINITY).max(1));
            auto min_p = std::get<0>(padded.nan_to_num(INFINITY).min(1));
            auto b_val_est_new = torch::where(b_playing0, max_p, min_p);
            
            auto loss = (b_val_est_new - b_val_est_old).square().mean();
            loss.backward();
        }

        for (const auto &p : model.named_parameters())
        {
            auto adj = -p.value.grad();
            param_sum_adj[p.name].add_(adj);
            param_sum_abs_adj[p.name].add_(adj.abs());
            p.value.data().add_(config::learning_rate * param_lr[p.name] * adj);
        }
    }
}

namespace Episode
{
    std::chrono::high_resolution_clock::time_point start;

    state curr_state;
    encoded_state enc_curr_state;
    int sgd_step_skip;
    std::vector<at::Tensor> choosen;

    std::mutex tr_mutex;
    std::vector<int> tr_id;
    std::vector<float> tr_score_delta;
    std::vector<encoded_state> tr_new_state;

    inline void clear_transitions()
    {
        std::lock_guard lock(tr_mutex);
        tr_id.clear();
        tr_score_delta.clear();
        tr_new_state.clear();
    }
}

void model_new_episode(model_server::state &initial_state)
{
    torch::GradMode::set_enabled(false);

    Episode::start = std::chrono::high_resolution_clock::now();
    Episode::clear_transitions();

    Episode::curr_state = initial_state;
    Episode::enc_curr_state = encode_state(Episode::curr_state);
    Episode::sgd_step_skip = config::sgd_step_skip;
    Episode::choosen.clear();
}

void model_new_state(state &new_state)
{
    if (config::train)
    {
        ReplayMemory::store(Episode::enc_curr_state, Episode::tr_score_delta, Episode::tr_new_state, Episode::curr_state.playing);
        if (--Episode::sgd_step_skip == 0)
        {
            Episode::sgd_step_skip = config::sgd_step_skip;
            ReplayMemory::sgd_step();
        }
    }

    Episode::clear_transitions();
    Episode::curr_state = new_state;
    Episode::enc_curr_state = encode_state(new_state);
}

void model_new_transition_task(transition tr)
{
    torch::GradMode::set_enabled(false);

    auto enc_new_state = encode_state(tr.new_state);
    float score_delta = tr.new_state.score[0] - tr.new_state.score[1] - Episode::curr_state.score[0] + Episode::curr_state.score[1];
    {
        std::lock_guard lock(Episode::tr_mutex);
        Episode::tr_id.push_back(tr.id);
        Episode::tr_score_delta.push_back(score_delta);
        Episode::tr_new_state.push_back(enc_new_state);
    }
}

void model_new_transition(transition &new_transition)
{
    pool.push_task(model_new_transition_task, new_transition);
}

int model_choose_transition()
{
    pool.wait_for_tasks();
    int size = Episode::tr_new_state.size();

    auto stacked_s_new = stack_encoded_states(Episode::tr_new_state);
    auto val_est = model_forward(stacked_s_new);
    auto score_delta = cuda(torch::from_blob(Episode::tr_score_delta.data(), {size}, FCPU));
    auto tr_val_est = score_delta / config::score_factor + config::discount_factor * val_est;

    int index;
    if (config::deterministic[Episode::curr_state.playing] || config::epsilon < unif01(rng))
    {
        if (Episode::curr_state.playing == 0)
            index = tr_val_est.argmax().to(torch::kCPU).item<int64_t>();
        else
            index = tr_val_est.argmin().to(torch::kCPU).item<int64_t>();
    }
    else
        index = rng() % size;

    Episode::choosen.push_back(tr_val_est.index({index}).unsqueeze(0));
    return Episode::tr_id[index];
}

void model_end_episode(model_server::state &final_state)
{
    if (config::train)
    {
        ReplayMemory::store(Episode::enc_curr_state, Episode::tr_score_delta, Episode::tr_new_state, Episode::curr_state.playing);
        if (--Episode::sgd_step_skip == 0)
        {
            Episode::sgd_step_skip = config::sgd_step_skip;
            ReplayMemory::sgd_step();
        }
    }

    Episode::clear_transitions();
    Episode::curr_state = final_state;
    Episode::enc_curr_state = encode_state(final_state);

    if (config::train)
    {
        // Normalement les deux derniers arguments sont vides (car état final)
        ReplayMemory::store(Episode::enc_curr_state, Episode::tr_score_delta, Episode::tr_new_state, Episode::curr_state.playing);
        ReplayMemory::sgd_step();
    }

    auto dur = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - Episode::start).count() / 1000.;
    printf("Episode ended in %.2fs", dur);
    printf(" with scores  %d  %d", final_state.score[0], final_state.score[1]);
    auto choices = torch::cat(Episode::choosen).cpu();
    double avgq = choices.mean().item<double>(), stdq = choices.std().item<double>();
    printf("  AvgQ=%.3g  StdQ=%.3g", avgq, stdq);
    puts("");
}

void model_adjust_lr()
{
    for (const auto &p : model.named_parameters())
        param_lr[p.name] = param_sum_adj[p.name].abs() / (param_sum_abs_adj[p.name] + 1e-20f);
}

void model_show_stats()
{
    std::vector<at::Tensor> params_vec;
    for (const auto &p : param_lr)
        params_vec.push_back(p.second.detach().flatten());
    auto params = torch::cat(params_vec);
    auto rel_mean = params.mean().to(torch::kCPU).item<float>();
    auto rel_std = params.std().to(torch::kCPU).item<float>();
    printf("Auto LR:  mean %.4g  stddev %.4g\n", config::learning_rate * rel_mean, config::learning_rate * rel_std);
    printf("Replay memory:  %d / %d\n", (int)(ReplayMemory::mem_l.size() + ReplayMemory::mem_r.size()), config::rmem_size);
}