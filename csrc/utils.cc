#include "api.hh"
#include "utils.hh"
#include <algorithm>
#include <unordered_set>
#include <vector>
using namespace std;

vector<pair<position, int>> compute_regions()
{
    unordered_set<position> seen;
    vector<pair<position, int>> regions;
    for (position pos0 : POS)
    {
        case_type elem = type_case(pos0, moi());
        if (elem != VIDE && seen.count(pos0) == 0)
        {
            auto pr = positions_region(pos0, moi());
            regions.push_back(make_pair(pos0, pr.size()));
            for (position pos : pr)
                seen.insert(pos);
        }
    }
    return regions;
}
