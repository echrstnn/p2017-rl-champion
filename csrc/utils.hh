#pragma once

#include "api.hh"
#include <utility>
#include <vector>

std::vector<std::pair<position, int>> compute_regions();